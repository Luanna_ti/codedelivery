<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Entities\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Entities\Category::class, function (Faker $faker) {
    return [
        'name' => $faker->name,

    ];
});
$factory->define(App\Entities\Product::class, function (Faker $faker) {
    return [

        'name' => $faker->word,
        'description'=> $faker->sentence,
        'price'=> $faker->numberBetween(10, 50)

    ];
});

$factory->define(App\Entities\Client::class, function (Faker $faker) {
    return [

        'phone' => $faker->phoneNumber,
        'address' => $faker->address,
        'city' => $faker->city,
        'state' => $faker->state,
        'zipcode' => $faker->postcode


    ];
});
$factory->define(App\Entities\Order::class, function (Faker $faker) {
        return [
            'client_id' => rand(1,10),
            'total' => rand(50,100),
            'status' => 0
        ];
});
$factory->define(App\Entities\OrderItem::class, function (Faker $faker) {
        return [
            'product_id' => rand(1,10),
            'order_id' => rand(1,10),
            'price' => rand(1,10),
            'qtd' => rand(1,10),
        ];
});
$factory->define(App\Entities\Cupom::class, function (Faker  $faker) {
    return [
        'code' => rand(100,1000),
        'value' => rand(50,100)
    ];
});