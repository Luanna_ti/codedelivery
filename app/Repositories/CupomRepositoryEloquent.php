<?php

namespace App\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\CupomRepository;
use App\Entities\Cupom;
use App\Validators\CupomValidator;

/**
 * Class CupomRepositoryEloquent.
 *
 * @package namespace App\Repositories;
 */
class CupomRepositoryEloquent extends BaseRepository implements CupomRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    protected $skipPresenter = true;
    public function model()
    {
        return Cupom::class;
    }

    /**
     * Boot up the repository, pushing criteria
     * @throws \Prettus\Repository\Exceptions\RepositoryException
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }

    /**
     * @return string
     */

    public function findByCode($code)
    {
        // TODO: Implement findByCode() method.
        $result = $this->model
            ->where('code', $code)
            ->where('used',0)
            ->first();
        if($result){
            return $this->parserResult($result);
        }
        throw (new ModelNotFoundException)->setModel(get_class($this->model));
    }
}