<?php

namespace App\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface OrderRepository.
 *
 * @package namespace App\Repositories;
 */
interface OrderRepository extends RepositoryInterface
{
    /**
     * @param $id
     * @param $idDeliveryman
     * @return mixed
     */
    public function getByIdAndDeliveryman( $id, $idDeliveryman);
}
