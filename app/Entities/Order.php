<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Order.
 *
 * @package namespace App\App\Entities;
 */
class Order extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

        'client_id',
        'user_deliveryman_id',
        'total',
        'status',
        'cupom_id'
        ];

    public function client()
    {
        return $this->belongsTo(Client::class);
    }

    public function cupom()
    {
        return $this->belongsTo(Cupom::class);
    }

    public function items()
    {
        return $this->hasMany(OrderItem::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }
}