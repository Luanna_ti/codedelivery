<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
/**
 * Class User.
 *
 * @package namespace App\App\Entities;
 */
class User extends Authenticatable implements Transformable
{
    use TransformableTrait;

    use Notifiable;


    protected $fillable = [
        'name',
        'email',
        'password',
        'role'
    ];


    protected $hidden = [
        'password', 'remember_token','role'];

    public function hasPermission($permission)
    {
        $permissions = [
            'client.orders.index'
        ];

        return in_array($permission,$permissions) ? true : false;
    }
    public function isSuperAdmin()
    {
        return $this->role == "admin" ? true : false ;
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function client()
    {
        return $this->hasOne(Client::class);
    }

}
