<?php

namespace App\Providers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(GateContract $gate, Request $request )
    {
        $this->registerPolicies();
        $gate->before(function($user)
        {
            Gate::define('admin.categories.index', function ($user) {
                return $user->hasPermission('admin.categories.index');
            });
            Gate::define('admin.products.index', function ($user) {
                return $user->hasPermission('admin.categories.index');
            });
            Gate::define('admin.clients.index', function ($user) {
                return $user->hasPermission('admin.categories.index');
            });
            Gate::define('client.orders.index', function ($user) {
                return $user->hasPermission('client.orders.index');
            });
            if ($user->isSuperAdmin()){
                return true;
            }
        });
    }
}
