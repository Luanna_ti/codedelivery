<?php

namespace App\Http\Controllers;



use App\Http\Requests\AdminCategoryRequest;
use App\Repositories\CategoryRepository;


/**
 * Class CategoriesController.
 *
 * @package namespace App\Http\Controllers;
 */
class CategoriesController extends Controller
{
    /**
     * @var CategoryRepository
     */
    private $repository;

    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(){
        $categories = $this->repository->paginate(5);

        return view('admin.categories.index', compact('categories'));
    }

    public function create(){
        return view('admin.categories.create');
    }

    public function store(AdminCategoryRequest $request){
        $data= $request->all();
        $this->repository->create($data);

        return redirect()->route('admin.categories.index')->with('message', 'Categoria Cadastrada com sucesso!');
    }

    public function edit($id){
        $category = $this->repository->find($id);
        return view('admin.categories.edit', compact('category'));



    }


    /**
     * @param AdminCategoryRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update( AdminCategoryRequest $request, $id)
    {
        $data= $request->all();
        $this->repository->update($data, $id);

        return redirect()->route('admin.categories.index')->with('message', 'Categoria editada com sucesso!');

    }
    public function destroy($id){
        $this->repository->delete($id);
        return redirect()->route('admin.categories.index')->with('message', 'Categoria removida com sucesso!');
    }
}