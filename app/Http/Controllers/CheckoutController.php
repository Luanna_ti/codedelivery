<?php

namespace App\Http\Controllers;



use App\Http\Requests\CheckoutRequest;
use App\Repositories\OrderRepository;
use App\Repositories\ProductRepository;
use App\Repositories\UserRepository;
use App\Services\OrderService;
use Illuminate\Support\Facades\Auth;


/**
 * Class CategoriesController.
 *
 * @package namespace App\Http\Controllers;
 */


class CheckoutController extends Controller
{
    private $repository;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var ProductRepository
     */
    private $productRepository;
    /**
     * @var OrderService
     */
    private $orderService;
    /**
     * @var OrderService
     */
    private $service;

    public function __construct(


        OrderRepository $repository,
        UserRepository $userRepository,
        ProductRepository $productRepository,
        OrderService $service)
    {
        $this->repository = $repository;
        $this->userRepository = $userRepository;
        $this->productRepository = $productRepository;
        $this->service = $service;
    }
    public function index(){
        $clientId = Auth::id();
        $orders = $this->repository->scopeQuery(function($query) use($clientId){

            return $query->where('client_id', '=', $clientId);
        })->paginate();

        return view('customer.order.index', compact('clientId','orders'));
    }
    public function create()
    {
        $products = $this->productRepository->get();
        return view('customer.order.create',compact('products'));
    }



    public function store(CheckoutRequest $request){
        error_log( Auth::id());
        $data = $request->all();
        error_log(Auth()->user()->id);
        $clientId = Auth::id();
        $data['client_id'] = $clientId;
        $this->service->create($data);
        return redirect()->route('customer.order.index');
    }
}