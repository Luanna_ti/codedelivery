<?php

namespace App\Http\Controllers;



use App\Http\Requests\AdminProductRequest;

use App\Repositories\ProductRepository;
use App\Repositories\CategoryRepository;


/**
 * Class CategoriesController.
 *
 * @property  CategoryRepository
 * @property ProductRepository repository
 * @package namespace App\Http\Controllers;
 */
class ProductsController extends Controller
{
    private $repository;
    /**
     * @var CategoryRepository
     */
    private $categoryRepository;
    public function __construct(ProductRepository $repository, CategoryRepository $categoryRepository)
    {
        $this->repository = $repository;
        $this->categoryRepository = $categoryRepository;
    }
    public function index(){
        $products = $this->repository->paginate(5);
        return view('admin.products.index', compact('products'));
    }
    public function create()
    {
        $categories = $this->categoryRepository->get();
        return view('admin.products.create', compact('categories'));
    }
    public function store(AdminProductRequest $request){
        $data = $request->all();
        $this->repository->create($data);
        //dd($request->all());
        return redirect()->route('admin.products.index');
    }
    public function edit($id){
        $product = $this->repository->find($id);
        $categories = $this->categoryRepository->get();
        return view('admin.products.edit',compact('product','categories'));
    }

    /**
     * @param AdminProductRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update( AdminProductRequest $request, $id){
        $data = $request->all();
        $this->repository->update($data, $id);
        return redirect()->route('admin.products.index')->with('message', 'Produto editado com sucesso!');
    }
    public function destroy($id){
        $this->repository->delete($id);
        return redirect()->route('admin.products.index')->with('message', 'Produto excluido com sucesso!');
    }
}